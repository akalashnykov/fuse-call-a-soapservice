package nl.rubix.service.CallSOAPService;

import nl.rubix.service.xsd.neworder.GetOrderResponse;
import nl.rubix.service.xsd.neworder.GetTheSpecialOrderRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ProcessResponse implements Processor{
	
	private Logger log = LoggerFactory.getLogger("test");

	@Override
	public void process(Exchange exchange) throws Exception {
		GetOrderResponse response = exchange.getIn().getBody(GetOrderResponse.class);
		log.info("reponse: " + response.getOrderNumber().toString());
		
		
	}

}
