package nl.rubix.service.CallSOAPService;

import java.math.BigInteger;

import nl.rubix.service.xsd.neworder.GetTheSpecialOrderRequest;
import nl.rubix.service.xsd.neworder.ObjectFactory;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.CxfConstants;

public class NewOrderProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		
		//exchange.getOut().setHeader("OperationName", "getTheSpecialOrder");
		
		ObjectFactory objFac = new ObjectFactory();
		GetTheSpecialOrderRequest requestMsg = objFac.createGetTheSpecialOrderRequest();
		requestMsg.setOrderNumber(BigInteger.valueOf(123));
		exchange.getOut().setBody(requestMsg);
		
	}
	

}
